var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var gutil = require('gulp-util');
var babelify = require('babelify');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var envify = require('envify/custom');

var dependencies = [
    'react',
    'react-dom'
];
var scriptsCount = 0;

gulp.task('apply-prod-environment', function () {
    //process.env.NODE_ENV = 'development';
});

gulp.task('scripts', function () {
    bundleApp(false);
});

gulp.task('deploy', function () {
    bundleApp(true);
});

// gulp.task('watch', function () {
//     gulp.watch(['./app/*.js'], ['scripts']);
// });

gulp.task('default', ['apply-prod-environment', 'scripts' /*, 'watch' */]);
function bundleApp(isProduction) {
    scriptsCount++;
    var appBundler = browserify({
        entries: './app/app.js',
        debug: true
    })
    if (!isProduction && scriptsCount === 1) {
        browserify({
            require: dependencies,
            debug: true
        })
            .bundle()
            .on('error', gutil.log)
            .pipe(source('vendors.js'))
            .pipe(buffer())
            .pipe(uglify())
            .pipe(gulp.dest('./web/js/'));
    }
    if (!isProduction) {
        dependencies.forEach(function (dep) {
            appBundler.external(dep);
        })
    }

    appBundler
        .transform("babelify", { presets: ["es2015", "react"] })
        .bundle()
        .on('error', gutil.log)
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('./web/js/'));
}